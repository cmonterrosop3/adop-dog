import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Post } from '../services/post/post.interface';
import { Request } from '../services/request/request.interface';
import { FirebaseService } from '../services/firebase/firebase.service';
import { RequestService } from '../services/request/request.service';
import { AlertService } from '../services/alert/alert.service';
import { PostService } from '../services/post/post.service';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-view-request',
  templateUrl: './view-request.page.html',
  styleUrls: ['./view-request.page.scss'],
})
export class ViewRequestPage implements OnInit {
  request: Request;
  post: Post;
  userId: string;
  requestId: string;
  type: string;
  constructor(
    private activateRouter: ActivatedRoute,
    private firebase: FirebaseService,
    private postService: PostService,
    private requestService: RequestService,
    private alert: AlertService,
    private router: Router,
    private callNumber: CallNumber,
  ) {
    this.request = {} as Request;
    this.post = {} as Post;
    this.userId = window.localStorage.getItem('userId');
    this.requestId= this.activateRouter.snapshot.paramMap.get('requestId');
    this.type= this.activateRouter.snapshot.paramMap.get('type');
  }

  async ngOnInit() {
   this.getRequest();
  }

  async getRequest() {
    await this.requestService.getRequest(this.requestId)
    .then(async req => {
      console.log('data firebase request: ', req.data())
      this.request = req.data();
      this.request.id = req.id;
      const prePost = await this.postService.getPost(this.request.postId);
      this.post = prePost.data();
      console.log('data firebase post: ', this.post)
      return this.request
    } , async error => {
      await this.alert.presentAlertError('Error', error.message);
    })
  }

  async agreeRequest() {

    return this.alert.presentAlertConfirm('¿Está seguro de aceptar la solicitud?', () => {
      this.requestService.updateRequest('Aceptada', this.request.id)
      .then(async (data) => {
        console.log('registro request actualizar agree: ', data)
        await this.postService.updatePost('Inactivo', this.request.postId);
        return this.alert.presentAlertInfo('Estado del registro', 'La solicitud fue aceptada')
        .then(() => {
          return this.router.navigate(['/home/']);
        })
      }, async error => {
        await this.alert.presentAlertError('Error', error.message);
      })
    })
  }

  async rejectRequest() {
    return this.alert.presentAlertConfirm('¿Está seguro de rechazar la solicitud?', () => {
      this.requestService.updateRequest('Rechazada', this.request.id)
      .then(async (data) => {
        console.log('registro request actualizar reject: ', data)
        return this.alert.presentAlertInfo('Estado del registro', 'La solicitud fue rechazada')
        .then(() => {
          return this.router.navigate(['/home/']);
        })
      }, async error => {
        await this.alert.presentAlertError('Error', error.message);
      })
    })
  }

  public callPhone( tel: string ) {
    this.callNumber.callNumber(tel, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(async err => await this.alert.presentAlertError('Error', err.message));
  }

}
