import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Post } from '../services/post/post.interface';
import { FirebaseService } from '../services/firebase/firebase.service';
import { PostService } from '../services/post/post.service';
import { AlertService } from '../services/alert/alert.service';
import { Race } from '../services/post/race.interface';
import { ImageService } from '../services/image/image.service';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.page.html',
  styleUrls: ['./create-post.page.scss'],
})
export class CreatePostPage implements OnInit {
  post: Post;
  userId: string;
  departments: {id: string; department: string}[]
  municipalities: {id: string; municipality: string; department: string}[]
  races: Race []
  constructor(
    private firebase: FirebaseService,
    private postService: PostService,
    private imageService: ImageService,
    private alert: AlertService,
    private router: Router
  ) {
    this.post = {} as Post;
    this.departments = [];
    this.municipalities = [];
    this.races = [] as Race[];
    this.userId = window.localStorage.getItem('userId');
  }

  async ngOnInit() {
    await this.firebase.getDeparments()
    .then(data => {
      console.log('data firebase department: ', data)
      data.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data().department}`);
        this.departments.push({
          id: doc.id,
          department: doc.data().department
        })
      })
    }, async error => {
      await this.alert.presentAlertError('Error', error.message);
    })

    await this.firebase.getRaces()
    .then(data => {
      console.log('data firebase races: ', data)
      data.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data().race}`);
        this.races.push({
          id: doc.id,
          race: doc.data().race
        })
      })
    }, async error => {
      await this.alert.presentAlertError('Error', error.message);
    })

    
  }

  async getMunicipalities(departmentId: string) {
    this.municipalities = [];
    this.post.municipality = null;
    this.firebase.getMunicipalities(departmentId)
    .then(data => {
      console.log('data firebase municipality: ', data)
      data.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data().municipality}`);
        this.municipalities.push({
          id: doc.id,
          municipality: doc.data().municipality,
          department: doc.data().department,
        })
      })
    }, async error => {
      await this.alert.presentAlertError('Error', error.message);
    })
  }

  async save() {
    this.post.createdAt = new Date();
    this.post.updatedAt = new Date();
    this.post.createdBy = this.userId;
    this.post.updatedBy = this.userId;
    this.post.personId = this.userId;
    this.post.status = 'Activo';
    console.log('esto lleva post: ', this.post);
    this.postService.savePost(this.post)
    .then(async (data) => {
      console.log('registro post guardado: ', data)
      return this.alert.presentAlertInfo('Estado del registro', 'El post fue creado')
      .then(() => {
        return this.router.navigateByUrl('home');
      })
    }, async error => {
      await this.alert.presentAlertError('Error', error.message);
    })
  }

  takePhoto() {
    this.imageService.takePhoto()()
    .then((data: string) => {
      this.post.picture = data ? data : null;
      console.log('foto de camara: ', this.post.picture);
    })
    .catch( async (error) => {
      await this.alert.presentAlertError('Error', error.message);
    });
  }

}
