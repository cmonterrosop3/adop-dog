import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from '../services/alert/alert.service';
import { AuthService } from '../services/auth/auth.service';
import { Login } from '../services/auth/login.interface';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {

  form: Login;
  constructor(
    private authService: AuthService,
    private router: Router,
    private alert: AlertService
  ) {
    this.form = {} as Login;
  }

  ngOnInit() {
  }

  async signUpUser() : Promise<void>{
    this.authService.signUpUser(this.form)
    .then((data) => {
      console.log('trae el registro: ', data)
      window.localStorage.setItem('userId', data.user.uid);
      this.router.navigateByUrl('home');
    },
    async error => {
      const alert = await this.alert.presentAlertError('Error', 'Ocurrió un error en el registro, intentelo de nuevo');
    })
  }

}

