import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from '../services/alert/alert.service';
import { AuthService } from '../services/auth/auth.service';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.page.html',
  styleUrls: ['./password-reset.page.scss'],
})
export class PasswordResetPage implements OnInit {
  email: string;
  constructor(
    private authService: AuthService,
    private router: Router,
    private alert: AlertService
  ) {
    this.email = '';
  }

  ngOnInit() {
  }

  async resetPassword() : Promise<void>{
    this.authService.resetPassword(this.email)
    .then(async (data) => {
      console.log('trae el reset: ', data)
      this.alert.presentAlertInfo('Atención', 'Revisa tu bandeja de correo para resetear tu contraseña')
      this.router.navigateByUrl('login');
    },
    async error => {
      await this.alert.presentAlertError('Error', error.message);
    })
  }

}
