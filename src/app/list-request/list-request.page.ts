import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../services/alert/alert.service';
import { Request } from '../services/request/request.interface';
import { RequestService } from '../services/request/request.service';
@Component({
  selector: 'app-list-request',
  templateUrl: './list-request.page.html',
  styleUrls: ['./list-request.page.scss'],
})
export class ListRequestPage implements OnInit {
  requests: Request[];
  userId: string;
  type: string;
  constructor(
    private activateRouter: ActivatedRoute,
    private requestService: RequestService,
    private alert: AlertService
  ) {
    this.requests = [] as Request[];
    this.userId = window.localStorage.getItem('userId');
    this.type = this.activateRouter.snapshot.paramMap.get('type');
  }

  ngOnInit() {
  }

  async ionViewDidEnter() {
    this.requests = [] as Request[]
    console.log('entro getRequest')

    if (this.type == 'mine') {
      return await this.requestService.getRequestMine(this.userId)
      .then(data => {
        console.log('data firebase request: ', data)
        data.forEach(async (doc) => {
          let request: Request = doc.data() as Request
          request.id = doc.id;
          console.log(`${doc.id} => ${doc.data().personFullName}`);
          this.requests.push(request as Request)
        })
        
        console.log(`request list: `, this.requests);
      }, async error => {
        await this.alert.presentAlertError('Error', error.message);
      })
    } else {
      return await this.requestService.getRequestGot(this.userId)
      .then(data => {
        console.log('data firebase request: ', data)
        data.forEach(async (doc) => {
          let request: Request = doc.data() as Request
          request.id = doc.id;
          console.log(`${doc.id} => ${doc.data().personFullName}`);
          this.requests.push(request as Request)
        })
        
        console.log(`request list: `, this.requests);
      }, async error => {
        await this.alert.presentAlertError('Error', error.message);
      })
    }
    


  }

}
