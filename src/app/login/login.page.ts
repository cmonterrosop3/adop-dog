import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from '../services/alert/alert.service';
import { AuthService } from '../services/auth/auth.service';
import { Login } from '../services/auth/login.interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  form: Login;
  constructor(
    private authService: AuthService,
    private router: Router,
    private alert: AlertService
  ) {
    this.form = {} as Login;
  }

  ngOnInit() {
  }

  async loginUser() : Promise<void>{
    this.authService.loginUser(this.form)
    .then((data) => {
      console.log('trae el login: ', data)
      window.localStorage.setItem('userId', data.user.uid);
      this.router.navigateByUrl('home');
    },
    async error => {
      const alert = await this.alert.presentAlertError('Error', 'Ocurrió un error en la autenticación, intentelo de nuevo');
    })
  }

}
