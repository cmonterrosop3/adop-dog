import { Injectable } from '@angular/core';
import { query, where } from '@firebase/firestore';
import { FirebaseService } from '../firebase/firebase.service';
import { Request } from './request.interface';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(
    private firestoreService: FirebaseService
  ) { }

  public async getRequestMine(createdBy: string) {
    return (await this.firestoreService.firestoreInstance()).
    firebase.getDocs(query((await this.firestoreService.firestoreInstance())
    .firebase.collection(this.firestoreService.db, `request`), where('createdBy', '==', createdBy)))
  }

  public async getRequestGot(ownderPostId: string) {
    return (await this.firestoreService.firestoreInstance()).
    firebase.getDocs(query((await this.firestoreService.firestoreInstance())
    .firebase.collection(this.firestoreService.db, `request`), where('ownderPostId', '==', ownderPostId)))
  }

  public async getRequest(requestId: string) {
    return (await this.firestoreService.firestoreInstance()).firebase.getDoc(
      (await this.firestoreService.firestoreInstance())
      .firebase.doc(this.firestoreService.db, `request/${requestId}`))
  }

  async updateRequest(status: string, requestId: string) : Promise<any> {
    try {
      const docRef = await (await this.firestoreService.firestoreInstance())
      .firebase.doc(this.firestoreService.db, `request/${requestId}`)

      return (await this.firestoreService.firestoreInstance())
      .firebase.updateDoc(docRef, {
        status: status
      });   

    } catch (e) {
      console.error("Error adding document: ", e);
    }
  }

  async saveRequest(request: Request): Promise<string> {
    try {
      const docRef = await (await this.firestoreService.firestoreInstance())
      .firebase.addDoc((await this.firestoreService.firestoreInstance())
      .firebase.collection(this.firestoreService.db, "request"), request);
    
      console.log("Document written with ID: ", docRef.id);
      return docRef.id
    } catch (e) {
      console.error("Error adding document: ", e);
    }
  }
}
