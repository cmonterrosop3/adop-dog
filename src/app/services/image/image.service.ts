import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer/ngx';
import { Base64 } from '@ionic-native/base64/ngx';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(
    private camera: Camera,
    private imageResizer: ImageResizer,
    private base64: Base64,
  ) { }

  takePhoto = () => () =>
    new Promise<string>(async (resolve, reject) => {
      let quality = 100;
      let image: string;
      const options: CameraOptions = {
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 1000,
        targetHeight: 1000,
        quality
      };
      this.camera.getPicture( options )
      .then(async imageData => {
        image = `data:image/jpeg;base64,${imageData}`;
        while (((4 * Math.ceil(image.length / 3) * 0.5624896334383812) / 1000) > 600) {
          quality = quality - 3;
          let width, height;
          const newImage = await new Promise<any>((resolved, rejected) => {
            const temp = new Image();
            temp.onload = () => {
              width = temp.width > 1200 ? Math.ceil(temp.width * (1200 / temp.width)) : temp.width;
              height = temp.width > 1200 ? Math.ceil(temp.height * (1200 / temp.width)) : temp.height;
              quality = quality;
              resolved({width, height, quality});
            };
            temp.src = image;
          });

          const optResizer = {
            uri: image,
            quality: newImage.quality,
            width: newImage.width,
            height: newImage.height,
            base64: true
            } as ImageResizerOptions;
          await this.imageResizer
              .resize(optResizer)
              .then(async (res: string) => {
                image = res;
              })
              .catch(e => console.log('error al reducir tamaño', e ));
        }
        resolve(image);
      })
      .catch((error) => {
        if (error === 'No Image Selected') {
          reject({message: error});
        } else {
          reject(error);
        }
      });
    })
}
