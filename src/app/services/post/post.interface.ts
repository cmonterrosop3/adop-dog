export interface Post {
    id?: string|null;
    name?: string|null;
    age?: number|null;
    race?: string|null;
    picture?: string|null;
    observation?: string|null;
    department?: string|null;
    municipality?: string|null;
    personFullName?: string|null,
    personId?: string|null;
    status?: string|null;
    createdAt?: Date|null;
    createdBy?: string|null;
    updatedAt?: Date|null;
    updatedBy?: string|null;
}